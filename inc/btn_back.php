<p class="text-end pt-4 pb-4">
  <a href="#" class="button  rounded-pill text-light btn btn-primary btn-back"><i class="fa fa-arrow-left  me-2" aria-hidden="true"></i>Regresar atrás</a>
</p>
<script type="text/javascript">
  let btn_back = document.querySelector(".btn-back");

  btn_back.addEventListener('click', function(e) {
    e.preventDefault();
    history.back();
  });
</script>