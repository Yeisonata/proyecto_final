<!--Navbar-->
<nav class="navbar navbar-expand-lg bg-body-tertiary border  border-light-subtle nav-links ">


  <div class="container-fluid">
    <!--Logo-->
    
    <a class="navbar-brand" href="index.php?vista=home"><img src="./img/tienda.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Usuarios
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="index.php?vista=user_new">Nuevo</a></li>
            <li><a class="dropdown-item" href="index.php?vista=user_list">Lista</a></li>
            <li><a class="dropdown-item" href="index.php?vista=user_search">Buscar</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Categoria
          </a>
          <ul class="dropdown-menu">
            <li><a  class="dropdown-item" href="index.php?vista=categoria_new">Nuevo</a></li>
            <li><a class="dropdown-item" href="index.php?vista=categoria_list">Lista</a></li>
            <li><a class="dropdown-item" href="index.php?vista=categoria_search">Buscar</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Producto
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="index.php?vista=producto_new">Nuevo</a></li>
            <li><a class="dropdown-item" href="index.php?vista=producto_list">Lista</a></li>
            <li><a class="dropdown-item" href="index.php?vista=producto_categoria">Por Categoria</a></li>
            <li><a class="dropdown-item" href="index.php?vista=producto_search">Buscar</a></li>
          </ul>
        </li>
        
      </ul>
      <div class="d-flex" role="search">

        <a href="index.php?vista=user_update&user_id_up=<?php echo $_SESSION['id'] ;?>" class="btn btn-success rounded-pill m-2" type="submit"><i class="fa-solid fa-user fa-fade" style="color: #f2f2f2;"></i><i class="fa-regular  me-2"></i>Mi cuenta</a>
        <button class="btn btn-info rounded-pill m-2" type="submit"><a href="index.php?vista=logout" style="color:#f2f2f2;"  class="me-3"><i class="fa-solid fa-right-from-bracket" ></i>Salir</a></button>
      </div>
    </div>
  </div>
</nav>