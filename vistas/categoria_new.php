
<div class="container ms-1 pb-1 pt-1">
  <p class="h4">Categorias</p>

  <p class="h5">Nueva categoria</p>
</div>

<div class="container pb-5 pt5 ">
  <div class="form-rest mb-5 mt-5"></div>
  <br>

  <form action="./php/categoria_guardar.php" method="post" autocomplete="off" class="FormularioAjax">
    <div class="row">
      <div class="col pb-4">
        <label class="form-label"><i class="fa fa-tag pe-2" aria-hidden="true"></i>Nombre</label>
        <input type="text" class="form-control" name="categoria_nombre" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{4,50}" maxlength="50" required placeholder="Escribe la categoria">
      </div>

      <div class="col pb-3">
        <label class="form-label"><i class="fa fa-map-marker pe-2" aria-hidden="true"></i>Ubicacion</label>
        <input type="text" class="form-control" name="categoria_ubicacion" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{5,150}" maxlength="150" placeholder="Escribe la ubicacion">
      </div>
    </div>
    <p class="text-center">
			<button type="submit" class="btn btn-primary is-info rounded-pill"><i class="fa fa-paper-plane pe-2" aria-hidden="true"></i>Guardar</button>
		</p>
  </form>