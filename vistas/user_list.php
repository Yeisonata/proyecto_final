
<div class=".container-fluid mb-6">
  <p class="fs-1 fw-semibold">Usuarios</p>
  <h2 class="fs-2 fw-semibold">Lista de usuarios</h2>
</div>

<div class="container pb-6 pt-6">
  <?php
  require_once "./php/main.php";

  //Elimnar Usuario
  if (isset($_GET['user_id_del'])) {
    require_once "./php/usuario_eliminar.php";
  }

  if (!isset($_GET['page'])) {
    $pagina = 1;
  } else {
    $pagina = (int) $_GET['page'];
    if ($pagina <= 1) {
      $pagina = 1;
    }
  }
  $pagina = limpiar_cadena($pagina);
  $url = "index.php?vista=user_list&page=";
  $registros = 15;
  $busquedad = "";

  require_once "./php/usuario_lista.php";
  ?>

</div>