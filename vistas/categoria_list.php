
<div class=".container-fluid mb-5">
  <p class="h4">Categorias</p>
  <p class="h5">Lista de categorias</p>
</div>

<div class="container pb-5 pt-5">
  <?php
  require_once "./php/main.php";

  // //Elimnar Categoria
  if (isset($_GET['categoria_id_del'])) {
    require_once "./php/categoria_eliminar.php";
  }

  if (!isset($_GET['page'])) {
    $pagina = 1;
  } else {
    $pagina = (int) $_GET['page'];
    if ($pagina <= 1) {
      $pagina = 1;
    }
  }
  $pagina = limpiar_cadena($pagina);
  $url = "index.php?vista=categoria_list&page=";
  $registros = 15;
  $busquedad = "";

  require_once "./php/categoria_lista.php";
  ?>

