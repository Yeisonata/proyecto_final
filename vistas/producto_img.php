<div class="container ms-1">
	<p class="h4">Productos</p>
	<p class="h5">Actualizar imagen de producto</p>
</div>

<div class="container pb-1 pt-1">
	<?php
include "./inc/btn_back.php";

require_once "./php/main.php";

$id = (isset($_GET['producto_id_up'])) ? $_GET['producto_id_up'] : 0;

	$check_producto = conectado();
	$check_producto = $check_producto->query("SELECT * FROM producto WHERE producto_id='$id'");

	if ($check_producto->rowCount() > 0) {
		$datos = $check_producto->fetch();
	?>

		<div class="form-rest mb-2 mt-2"></div>

		<div class="col d-flex">
			<div class="row">
				<?php if(is_file("./img/producto/".$datos['producto_foto'])){ ?>
				<figure class="image mb-2">
					<p class="card" style="width: 200px;"><img src="./img/producto/<?php echo $datos['producto_foto']; ?>"></p>

				</figure>
				<form class="FormularioAjax" action="./php/producto_img_eliminar.php" method="POST" autocomplete="off">

				<input type="hidden" name="img_del_id" value="<?php echo $datos['producto_id']; ?>">

					<p class="pb-3">
						<button type="submit" class="button  btn btn-danger rounded">Eliminar imagen</button>
					</p>
				</form>
				<?php }else{?>

				<figure class="image mb-5">
					<p class="card" style="width: 100px;"><img src="./img/caja.png"></p>
				</figure>
				<?php }?>
			</div>


			<div class=" ">
				<form class="mb-6 text-center FormularioAjax" action="./php/producto_img_actualizar.php" method="POST" enctype="multipart/form-data" autocomplete="off">

					<h4 class="text-center mb-6"><?php echo $datos['producto_nombre']; ?></h4>
					<input type="hidden" name="img_up_id" value="<?php echo $datos['producto_id']; ?>">
					<div class=" pb-3">
						<label class="form-label   "><i class="bi bi-file-earmark-image pe-3 "></i>Foto o imagen del producto:</label>
						<span>JPG,JPEG,PNG(MAX 3MB)</span>
						<input type="file" class="form-control  form-control-sm " name="producto_foto" accept=".jpg .png .jpeg" aria-describedby="inputGroupFileAddon04" aria-label="Upload">

					</div>
					<p class="text-center">
						<button type="submit" class="button btn btn-success rounded ">Actualizar</button>
					</p>
				</form>
			</div>
		</div>
	<?php
	} else {
		include "./inc/error_alert.php";
	}
	$check_producto = null;
	?>
</div>