<div class="container ms-1 pb-1 pt-1">
  <p class="h4">Productos</p>

  <p class="h5">Nuevo Producto</p>
</div>

<div class="container pb-5 pt5 ">
  <div class="form-rest mb-5 mt-5">
  <?php
  require_once  "./php/main.php";

  ?>
  <form action="./php/producto_guardar.php" method="post" autocomplete="off" class="FormularioAjax" enctype="multipart/form-data">

    <div class="row">
      <div class="col pb-4">
        <label class="form-label"><i class="bi bi-upc-scan pe-2"></i>Codigo de barra</label>
        <input type="text" class="form-control" name="producto_codigo" pattern="[a-zA-Z0-9]{1,70}" maxlength="70" required placeholder="Escribe el codigo de barra">
      </div>

      <div class="col pb-3">
        <label class="form-label"><i class="bi bi-card-checklist pe-2"></i>Nombre</label>
        <input type="text" class="form-control" name="producto_nombre" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ().,$#\-\/  ]{1,70}" maxlength="70" placeholder="Escribe el nombre">
      </div>
    </div>
    <div class="row">
      <div class="col pb-4">
        <label class="form-label "><i class="bi bi-tags-fill pe-2"></i>Precio</label>
        <input type="text" class="form-control" name="producto_precio" pattern="[0-9.]{1,25}" maxlength="25" required placeholder="Escribe el precio">
      </div>
      <div class="col pb-3">
        <label class="form-label"><i class="bi bi-bar-chart pe-2"></i>Stock</label>
        <input type="text" class="form-control" name="producto_stock" pattern="[0-9 ]{1,25}" maxlength="25" placeholder="Escribe el stock">
      </div>
      <div class="col pb-3 ">
        <label class="form-label"><i class="bi bi-bookmarks pe-2"></i> Categoria</label>
        <select name="producto_categoria" class="form-select form-select-sm rounded-pill " aria-label=".form-select-sm example">
          <option selected>Seleccione una opcion:</option>
          <?php
          $categorias = conectado();
          $categorias = $categorias->query("SELECT * FROM categoria");
          if ($categorias->rowCount() > 0) {
            $categorias = $categorias->fetchAll();
            foreach ($categorias as $row) {
              echo '<option value="' . $row['categoria_id'] . '">' . $row['categoria_nombre'] . '</option>';
            }
          }
          $categorias = null;
          ?>
        </select>
      </div>
      <div class="row">
        <div class="col-auto">
          <label class="form-label"><i class="bi bi-file-earmark-image pe-2"></i>Foto o imagen del producto:</label>
          <span>JPG,JPEG,PNG(MAX 3MB)</span>
          <input type="file" class="form-control form-control-sm" name="producto_foto" accept=".jpg .png .jpeg" aria-describedby="inputGroupFileAddon04" aria-label="Upload">
          
        </div>
      </div>
    </div>
    <p class="text-center">
      <button type="submit" class="btn btn-primary is-info rounded-pill"><i class="fa fa-paper-plane pe-2" aria-hidden="true"></i>Guardar</button>
    </p>
  </form>
  </div>
  </div>
  
  