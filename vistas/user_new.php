<div class="container ms-4 pb-5 pt5">
  <p class="h4">Usuarios</p>

  <p class="h5">Nuevo usuario</p>
</div>

<div class="container pb-6 pt6 ">
  <div class="form-rest mb-6 mt-6"></div>
  <br>

  <form action="./php/usuario_guardar.php" method="post" autocomplete="off" class="FormularioAjax">
    <div class="row">
      <div class="col pb-4">
        <label class="form-label"><i class="fa fa-hashtag pe-2" aria-hidden="true"></i>Nombre</label>
        <input type="text" class="form-control" name="usuario_nombre" pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}" maxlength="40" required>
      </div>

      <div class="col pb-3">
        <label class="form-label"><i class="fa fa-hashtag pe-2" aria-hidden="true"></i>Apellidos</label>
        <input type="text" class="form-control" name="usuario_apellido" pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}" maxlength="40" required>
      </div>
    </div>
    <div class="row">
      <div class="col pb-3">
        <label class="form-label"><i class="fa fa-user-circle pe-2" aria-hidden="true"></i>Usuario</label>
        <input type="text" class="form-control" name="usuario_usuario" pattern="[a-zA-Z0-9]{4,20}" maxlength="20" required>
      </div>

      <div class="col pb-3">
        <label class="form-label"><i class="fa fa-envelope-o pe-2" aria-hidden="true"></i>Email</label>
        <input type="email" class="form-control"  name="usuario_email"  maxlength="70" >
      </div>
    </div>
    <div class="row">
      <div class="col pb-3">
        <label class="form-label"><i class="fa fa-unlock pe-2" aria-hidden="true"></i>Clave</label>
        <input type="password" class="form-control"name="usuario_clave_1" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100" required>
      </div>

      <div class="col pb-3">
        <label class="form-label"><i class="fa fa-unlock pe-2" aria-hidden="true"></i>Repetir Clave</label>
        <input type="password" class="form-control"name="usuario_clave_2" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100" required>
      </div>
    </div>
    <p class="text-center">
			<button type="submit" class="btn btn-primary is-info rounded-pill">Guardar</button>
		</p>
  </form>
</div>

