<div class="container-md  ">
  <form class=" login" action="" method="post">
    <center>
      <p class="fs-3 fw-bold texto style">Hola de nuevo👋</p>
      <h5 class="  ">SISTEMA DE INVENTARIO</h5>
    </center>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label fw-bolder"><i class="fa fa-user pe-2 " aria-hidden="true"></i>Usuario</label><span class="text-danger "> * </span>
      <input class="form-control  border-primary" type="text" name="login_usuario" pattern="[a-zA-Z0-9]{4,20}" maxlength="20" required placeholder="Escribe tu usuario: ">

    </div>
    <div class="mb-4">
      <label for="exampleInputPassword1" class="form-label fw-bolder "><i class="fa fa-key pe-2" aria-hidden="true"></i>Contraseña</label><span class="text-danger "> * </span>
      <input class="form-control border-primary " type="password" name="login_clave" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100" required placeholder="Escribe tu contraseña">
    </div>

    <center> <button type="submit" class="btn btn-primary  form-control "><i class="fa fa-sign-out pe-2" aria-hidden="true"></i>Iniciar session</button></center>
    <?php
    if (isset($_POST['login_usuario']) && isset($_POST['login_clave'])) {
      require_once "./php/main.php";
      require_once "./php/iniciar_sesion.php";
    }
    ?>
  </form>
</div>