<?php
require_once "./php/main.php";

$id = (isset($_GET['categoria_id_up'])) ? $_GET['categoria_id_up'] : 0;
$id = limpiar_cadena($id);
?>
<div class="container-fluid mb-6">
  
    <p class="fs-2 fw-semibold">Categoria</p>
    <p class="fs-3 ">Actualizar Categoria</p>
</div>

<div class="container pb-3 pt-3">

  <?php

  include "./inc/btn_back.php";
  /*== Verificando categoria ==*/
  $check_categoria = conectado();
  $check_categoria= $check_categoria->query("SELECT * FROM categoria WHERE categoria_id='$id'");

  if ($check_categoria->rowCount() > 0) {
    $datos = $check_categoria->fetch();
  ?>

    <div class="form-rest mb-6 mt-6"></div>



    <form action="./php/actualizar_categoria.php" method="POST" class="FormularioAjax" autocomplete="off">

      <input type="hidden" value="<?php echo $datos['categoria_id']; ?>" name="categoria_id" required>

      <div class="row">
        <div class="col  pb-4">
          <div class="">
            <label class="form-label">Nombre</label>
            <input class="input form-control" type="text" name="categoria_nombre" value="<?php echo $datos['categoria_nombre']; ?>" pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}" maxlength="40" required>
          </div>
        </div>

        <div class="col pb-3">
          <label class="form-label">Ubicacion</label>
          <input class="input form-control" type="text" name="categoria_ubicacion" value="<?php echo $datos['categoria_ubicacion']; ?> " pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}" maxlength="40" required>
        </div>

      </div>
      
      <p class="text-center">
        <button type="submit" class="button  btn btn-success rounded-pill">Actualizar</button>
      </p>
    </form>


  <?php
  } else {
    include "./inc/error_alert.php";
  }
  $check_categoria = null;
  ?>
</div>