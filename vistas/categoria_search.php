<div class="container-fluid mb-4">

    <p class="fs-2 fw-semibold">Categorias</p>
    <p class="fs-3 ">Buscar categoria</p>

</div>

<div class="container pb-5 pt-5">
    <?php
    require_once "./php/main.php";

    if (isset($_POST['modulo_buscador'])) {
        require_once './php/buscador.php';
    }
    if (!isset($_SESSION['busqueda_Categoria']) && empty($_SESSION['busqueda_categoria'])) {
    ?>
        <div class="col">
            <div class="col">
                <form action="" method="POST" autocomplete="off">
                    <input type="hidden" name="modulo_buscador" value="categoria">
                    <div class="input-group">

                        <input class="form-control  rounded-pill" type="text" name="txt_buscador" placeholder="¿Qué estas buscando?" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{1,30}" maxlength="30">

                    </div>
                </form>
            </div>
        </div>
    <?php } else {  ?>
        <div class="rows">
            <div class="row">
                <form class="text-center mt-4 mb-4" action="" method="POST" autocomplete="off">
                    <input type="hidden" name="modulo_buscador" value="categoria">
                    <input type="hidden" name="eliminar_buscador" value="categoria">
                    <p>Estas buscando <strong>“<?php echo $_SESSION['busqueda_categoria']; ?>”</strong></p>
                    <br>
                    <button type="submit" class="button btn btn-danger rounded-pill">Eliminar busqueda</button>
                </form>
            </div>
        </div>


    <?php
        // if (isset($_GET['categoria_id_del'])) {
        //     require_once "./php/usuario_eliminar.php";
        // }

        if (!isset($_GET['page'])) {
            $pagina = 1;
        } else {
            $pagina = (int) $_GET['page'];
            if ($pagina <= 1) {
                $pagina = 1;
            }
        }
        $pagina = limpiar_cadena($pagina);
        $url = "index.php?vista=categoria_search&page=";
        $registros = 15;
        $busqueda = $_SESSION['busqueda_categoria'];

        require_once "./php/categoria_lista.php";
    }  ?>
</div>