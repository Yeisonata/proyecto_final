<div class="container is-fluid mb-6">
  <p class="fs-2 fw-semibold">Productos</p>
  <p class="fs-3 ">Lista de productos</p>
</div>

<div class="container pb-6 pt-6">
  <?php
      require_once "./php/main.php";

      # Eliminar producto #
      if(isset($_GET['producto_id_del'])){
          require_once "./php/producto_eliminar.php";
      }

      if(!isset($_GET['page'])){
          $pagina=1;
      }else{
          $pagina=(int) $_GET['page'];
          if($pagina<=1){
              $pagina=1;
          }
      }

      $categoria_id = (isset($_GET['categoria_id'])) ? $_GET['categoria_id'] : 0;

      $pagina=limpiar_cadena($pagina);
      $url="index.php?vista=producto_list&page="; /* <== */
      $registros=15;
      $busqueda="";

      # Paginador producto #
      require_once "./php/producto_lista.php";
  ?>
</div>
