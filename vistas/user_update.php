<?php
require_once "./php/main.php";

$id = (isset($_GET['user_id_up'])) ? $_GET['user_id_up'] : 0;
$id = limpiar_cadena($id);
?>
<div class="container-fluid mb-6">
  <?php if ($id == $_SESSION['id']) { ?>
    <p class="fs-2 fw-semibold">Mi cuenta</p>
    <p class="fs-3 ">Actualizar datos de cuenta</p>
  <?php } else { ?>
    <p class="fs-2 fw-semibold">Usuarios</p>
    <p class="fs-3 ">Actualizar usuario</p>
  <?php } ?>
</div>

<div class="container pb-3 pt-3">

  <?php

  include "./inc/btn_back.php";
  /*== Verificando usuario ==*/
  $check_usuario = conectado();
  $check_usuario = $check_usuario->query("SELECT * FROM usuario WHERE usuario_id='$id'");

  if ($check_usuario->rowCount() > 0) {
    $datos = $check_usuario->fetch();
  ?>

    <div class="form-rest mb-6 mt-6"></div>



    <form action="./php/actualizar_usuario.php" method="POST" class="FormularioAjax" autocomplete="off">

      <input type="hidden" value="<?php echo $datos['usuario_id']; ?>" name="usuario_id" required>

      <div class="row">
        <div class="col  pb-4">
          <div class="">
            <label class="form-label">Nombres</label>
            <input class="input form-control" type="text" name="usuario_nombre" value="<?php echo $datos['usuario_nombre']; ?>" pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}" maxlength="40" required>
          </div>
        </div>

        <div class="col pb-3">
          <label class="form-label">Apellidos</label>
          <input class="input form-control" type="text" name="usuario_apellido" value="<?php echo $datos['usuario_apellido']; ?> " pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}" maxlength="40" required>
        </div>

      </div>
      <div class="row">
        <div class="col pb-3">
          <label>Usuario</label>
          <input class="input form-control " type="text" name="usuario_usuario" value="<?php echo $datos['usuario_usuario']; ?> " pattern="[a-zA-Z0-9]{4,20}" maxlength="20" required>

        </div>

        <div class="col pb-3">
          <label>Email</label>
          <input class="input form-control" type="email" name="usuario_email" value="<?php echo $datos['usuario_email']; ?>" maxlength="70">
        </div>

      </div>
      <br><br>
      <p class="text-center">
        SI desea actualizar la clave de este usuario por favor llene los 2 campos. Si NO desea actualizar la clave deje los campos vacíos.
      </p>
      <br>
      <div class="row">
        <div class="col  pb-3">
          <label>Clave</label>
          <input class="input form-control" type="password" name="usuario_clave_1" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100">
        </div>

        <div class="col pb-3">
          <label>Repetir clave</label>
          <input class="input form-control" type="password" name="usuario_clave_2" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100">
        </div>

      </div>
      <br><br><br>
      <p class="text-center">
        Para poder actualizar los datos de este usuario por favor ingrese su USUARIO y CLAVE con la que ha iniciado sesión
      </p>
      <div class="row">
        <div class="col  pb-3">
          <label>Usuario</label>
          <input class="input form-control" type="text" name="administrador_usuario" pattern="[a-zA-Z0-9]{4,20}" maxlength="20" required>
        </div>

        <div class="col  pb-3">
          <label>Clave</label>
          <input class="input form-control" type="password" name="administrador_clave" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100" required>
        </div>

      </div>
      <p class="text-center">
        <button type="submit" class="button  btn btn-success rounded-pill">Actualizar</button>
      </p>
    </form>


  <?php
  } else {
    include "./inc/error_alert.php";
  }
  $check_usuario = null;
  ?>
</div>