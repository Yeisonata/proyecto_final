<div class="container-fluid mb-4">

    <p class="fs-2 fw-semibold">Productos</p>
    <p class="fs-3 ">Buscar productos</p>

</div>

<div class="container pb-5 pt-5">
    <?php
    require_once "./php/main.php";

    if (isset($_POST['modulo_buscador'])) {
        require_once './php/buscador.php';
    }
    if (!isset($_SESSION['busqueda_producto']) && empty($_SESSION['busqueda_producto'])) {
    ?>
        <div class="col">
            <div class="col">
                <form action="" method="POST" autocomplete="off">
                    <input type="hidden" name="modulo_buscador" value="producto">
                    <div class="input-group">

                        <input class="form-control  rounded-pill" type="text" name="txt_buscador" placeholder="¿Qué estas buscando?" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{1,30}" maxlength="30">

                    </div>
                </form>
            </div>
        </div>
    <?php } else {  ?>
        <div class="rows">
            <div class="row">
                <form class="text-center mt-4 mb-4" action="" method="POST" autocomplete="off">
                    <input type="hidden" name="modulo_buscador" value="producto">
                    <input type="hidden" name="eliminar_buscador" value="producto">
                    <p>Estas buscando <strong>“<?php echo $_SESSION['busqueda_producto']; ?>”</strong></p>
                    <br>
                    <button type="submit" class="button btn btn-danger rounded-pill">Eliminar busqueda</button>
                </form>
            </div>
        </div>


    <?php
        if (isset($_GET['producto_id_del'])) {
            require_once "./php/producto_eliminar.php";
        }

        if (!isset($_GET['page'])) {
            $pagina = 1;
        } else {
            $pagina = (int) $_GET['page'];
            if ($pagina <= 1) {
                $pagina = 1;
            }
        }
        $categoria_id = (isset($_GET['categoria_id'])) ? $_GET['categoria_id'] : 0;
        $pagina = limpiar_cadena($pagina);
        $url = "index.php?vista=producto_search&page=";
        $registros = 15;
        $busqueda = $_SESSION['busqueda_producto'];

        require_once "./php/producto_lista.php";
    }  ?>
</div>