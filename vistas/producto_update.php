<div class="container ms-1 ">
  <p class="h4">Productos</p>

  <p class="h5">Actualizar producto</p>
</div>
<div class="container pb-5 pt5 ">
  
  <?php
require_once "./php/main.php";
$id = (isset($_GET['producto_id_up'])) ? $_GET['producto_id_up'] : 0;
$id = limpiar_cadena($id);
  include "./inc/btn_back.php";
  /*== Verificando categoria ==*/
  $check_producto = conectado();
  $check_producto= $check_producto->query("SELECT * FROM producto WHERE producto_id='$id'");

  if ($check_producto->rowCount() > 0) {
    $datos = $check_producto->fetch();
  ?>
  <div class="form-rest mb-3 mt-3">
    <h2 class="text-center mb-3"><?php  echo $datos['producto_nombre']  ?></h2>
  <form action="./php/actualizar_producto.php" method="post" autocomplete="off" class="FormularioAjax" enctype="multipart/form-data">
  <input type="hidden" value="<?php echo $datos['producto_id']; ?>" name="producto_id" required>

    <div class="row">
      <div class="col pb-4">
        <label class="form-label"><i class="bi bi-upc-scan pe-2"></i>Codigo de barra</label>
        <input type="text" class="form-control" name="producto_codigo" pattern="[a-zA-Z0-9]{1,70}" maxlength="70" required value="<?php echo $datos['producto_codigo']; ?>">
      </div>

      <div class="col pb-3">
        <label class="form-label"><i class="bi bi-card-checklist pe-2"></i>Nombre</label>
        <input type="text" class="form-control" name="producto_nombre" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ().,$#\-\/  ]{1,70}" maxlength="70" value="<?php echo $datos['producto_nombre']; ?>">
      </div>
    </div>
    <div class="row">
      <div class="col pb-4">
        <label class="form-label "><i class="bi bi-tags-fill pe-2"></i>Precio</label>
        <input type="text" class="form-control" name="producto_precio" pattern="[0-9.]{1,25}" maxlength="25" required value="<?php echo $datos['producto_precio']; ?>">
      </div>
      <div class="col pb-3">
        <label class="form-label"><i class="bi bi-bar-chart pe-2"></i>Stock</label>
        <input type="text" class="form-control" name="producto_stock" pattern="[0-9 ]{1,25}" maxlength="25" value="<?php echo $datos['producto_stock']; ?>">
      </div>
      <div class="col pb-3 ">
        <label class="form-label"><i class="bi bi-bookmarks pe-2"></i> Categoria</label>
        <select name="producto_categoria" class="form-select form-select-sm rounded-pill " aria-label=".form-select-sm example">
          <option selected>Seleccione una opcion:</option>
          <?php
          $categorias = conectado();
          $categorias = $categorias->query("SELECT * FROM categoria");
          if ($categorias->rowCount() > 0) {
            $categorias = $categorias->fetchAll();
            foreach ($categorias as $row) {
              if ( $datos['categoria_id']==$row['categoria_id']) {
                echo '<option value="' . $row['categoria_id'] . '"selected="">' . $row['categoria_nombre'] . '   (Actual)</option>';
              } else {
                   
              echo '<option value="' . $row['categoria_id'] . '">' . $row['categoria_nombre'] . '</option>';
              }
            }
          }
          $categorias = null;
          ?>
        </select>
      </div>
      
    <p class="text-center">
      <button type="submit" class="btn btn-primary  rounded-pill"><i class="bi bi-box-arrow-down pe-2"></i>Guardar</button>
    </p>
  </form>
  </div>
  <?php
  } else {
    include "./inc/error_alert.php";
  }
  $check_producto = null;
  ?>
  </div>
  