<div class="container-fluid mb-6">
  <h1 class="fs-3 fw-semibold">Productos</h1>
  <h2 class="fs-4">Lista de productos por categoría</h2>
</div>

<div class="container pb-5 pt-5">
  <?php
  require_once  "./php/main.php";

  ?>
  <div class="col d-flex flex-row mb-3">
    <div class="">
      <h2 class="text-center ">Categorías</h2>


      <?php
      $categorias = conectado();
      $categorias = $categorias->query("SELECT * FROM categoria");
      if ($categorias->rowCount() > 0) {
        $categorias = $categorias->fetchAll();
        foreach ($categorias as $row) {
          
          echo '<a href="index.php?vista=producto_categoria&categoria_id=' . $row['categoria_id'] . '" class="button  text-decoration-none w-100">' . $row['categoria_nombre'] . '</a><br>';
        }
      } else {
        echo '  <p class="text-center">No hay categorías registradas</p>';
      }
      $categorias = null;
      ?>
    </div>
    <div class="col">
      <?php
      $categoria_id = (isset($_GET['categoria_id'])) ? $_GET['categoria_id'] : 0;
      $categoria = conectado();
      $categoria = $categoria->query("SELECT * FROM categoria where categoria_id=' $categoria_id '");
      if ($categoria->rowCount() > 0) {
        $categoria = $categoria->fetch();

        echo '
        <h2 class="text-center">' . $categoria['categoria_nombre'] . '</h2>
        <p class="text-center pb-5">' . $categoria['categoria_ubicacion'] . '</p>
        ';
        # Eliminar producto #
        if (isset($_GET['producto_id_del'])) {
          require_once "./php/producto_eliminar.php";
        }

        if (!isset($_GET['page'])) {
          $pagina = 1;
        } else {
          $pagina = (int) $_GET['page'];
          if ($pagina <= 1) {
            $pagina = 1;
          }
        }
        $pagina = limpiar_cadena($pagina);
        $url = "index.php?vista=producto_categoria&categoria_id=&page=$categoria_id"; 
        $registros = 15;
        $busqueda = "";

        # Paginador producto #
        require_once "./php/producto_lista.php";
      } else {
        echo '<h2 class="text-center" >Seleccione una categoría para empezar</h2>';
      }
      $categoria = null;
      ?>


    </div>
  </div>
</div>