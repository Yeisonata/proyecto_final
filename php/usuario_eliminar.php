<?php

$user_id_del = limpiar_cadena($_GET['user_id_del']);

//verficadon usario 
$check_usuario = conectado();
$check_usuario = $check_usuario->query("SELECT usuario_id FROM usuario WHERE usuario_id='$user_id_del'");
if ($check_usuario->rowCount() == 1) {

  $check_productos = conectado();
  $check_productos = $check_productos->query("SELECT usuario_id FROM producto WHERE usuario_id='$user_id_del' LIMIT 1");

  if ($check_productos->rowCount() <= 0) {

    $eliminar_usuario = conectado();
    $eliminar_usuario = $eliminar_usuario->prepare("DELETE FROM usuario WHERE usuario_id=:id");

    $eliminar_usuario->execute([":id" => $user_id_del]);

    if ($eliminar_usuario->rowCount() == 1) {
      echo '
        <div class="alert alert-info" role="alert">
        <strong>¡USUARIO  ELIMINADO </strong><br>
         El usuario se elimino  con exito
       </div>
        ';
    } else {
      echo '
          
            <div class="alert alert-warning" role="alert">
            <strong>¡Ocurrio un error inesperado</strong><br>
            No se pudo eliminar el usuario, por favor intente nuevamente
           </div>
        ';
    }
    $eliminar_usuario = null;
  } else {
    echo '
        
          <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
    No podemos eliminar el usuario ya que tiene productos registrados en el sistema
   </div>
      ';
  }
  $check_productos = null;
} else {
  echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
   El usuario no existe!!
   </div>
    ';
}
$check_usuario = null;
