<?php
require_once "main.php";
require_once "../inc/session_star.php";
#Almacenado de Datos$
$codigo = limpiar_cadena($_POST['producto_codigo']);
$nombre = limpiar_cadena($_POST['producto_nombre']);

$precio = limpiar_cadena($_POST['producto_precio']);
$stock = limpiar_cadena($_POST['producto_stock']);
$categoria = limpiar_cadena($_POST['producto_categoria']);

#Verificador de datos#

if ($codigo == "" || $nombre == "" || $precio == "" || $stock == "" || $categoria == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}
#Verificador De integridad de Datos

if (verificar_datos("[a-zA-Z0-9]{1,70}", $codigo)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El  Codigo  de barra no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ().,$#\-\/  ]{1,70}", $nombre)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Nombre no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[0-9.]{1,25}", $precio)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Precio no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[0-9 ]{1,25}", $stock)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Stock no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}

#Verificiar codigo#
$check_codigo = conectado();
$check_codigo = $check_codigo->query("SELECT producto_codigo	FROM producto WHERE producto_codigo='$codigo'");
if ($check_codigo->rowCount() > 0) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   El  CODIGO de BARRAS  ingresado ya se encuentra registrado,Verifique el codigo  ingresar nuevamente
 </div>
  ';
  exit();
}
//liberar espacio en  memoria
$check_codigo = null;

#Verificiar  Nombre#
$check_nombre = conectado();
$check_nombre = $check_nombre->query("SELECT 	producto_nombre FROM producto WHERE 	producto_nombre ='$nombre'");
if ($check_nombre->rowCount() > 0) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   El NOMBRE del PRODUCTO  ingresado ya se encuentra registrado,Verifique el usuario  ingresar nuevamente
 </div>
  ';
  exit();
}
//liberar espacio en  memoria
$check_nombre = null;

#Verificiar Categoria#
$check_categoria = conectado();
$check_categoria = $check_categoria->query("SELECT categoria_id	FROM categoria  WHERE categoria_id='$categoria'");
if ($check_categoria->rowCount() <= 0) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   La categoria seleccionada no existe.
 </div>
  ';
  exit();
}
//liberar espacio en  memoria
$check_categoria = null;


#Directorio de Imagenes# 
$img_dir  = "../img/producto/";

#comprobar si se selecciono una imagen

if ($_FILES['producto_foto']['name'] != "" && $_FILES['producto_foto']['size'] > 0) {
  #creando directorio 
  if (!file_exists($img_dir)) {
    // permisos de lectura y escritura
    if (!mkdir($img_dir, 0777)) {
      echo '
      <div class="alert alert-danger" role="alert">
      <strong>¡Ocurrio un error inesperado</strong><br>
      Error al crear el directorio
    </div>
      ';
      exit();
    }
  }
  //Verificar el formato de las imagenes
  if (mime_content_type($_FILES['producto_foto']['tmp_name']) != "image/jpeg" && mime_content_type($_FILES['producto_foto']['tmp_name']) != "image/png") {
    echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio   un error inesperado</strong><br>
   La imagen que ha seleccionado es de formato no permitido.
  </div>
    ';
    exit();
  }

  //Verificador del peso de las imagenes
  if (($_FILES['producto_foto']['size'] / 1024) > 3072) {
    echo '
    <div class="alert alert-danger" role="alert">
          <strong>¡Ocurrio   un error inesperado</strong><br>
            La imagen que ha seleccionado supera el peso  permitido.
    </div>
    ';
    exit();
  }
  //Para saber que tipo de imagen es
  switch(mime_content_type($_FILES['producto_foto']['tmp_name'])){
    case 'image/jpeg':
      $img_ext=".jpg";
    break;
    case 'image/png':
      $img_ext=".png";
    break;
  }

  //para dar permisos de lectura y escritura
  chmod($img_dir, 0777);
  $img_nombre = renombrar_fotos($nombre);
  $foto = $img_nombre . $img_ext;

  //Para mover la imagen al directorio
  if(!move_uploaded_file($_FILES['producto_foto']['tmp_name'], $img_dir.$foto)) {
    echo '
    <div class="alert alert-danger" role="alert">
          <strong>¡Ocurrio   un error inesperado</strong><br>
            No podemos subir la imagen al sistema en este momento.
    </div>
    ';
    exit();
  }
} else {
  $foto = "";
}

$guardar_producto=conectado();
    $guardar_producto=$guardar_producto->prepare("INSERT INTO producto(producto_codigo,producto_nombre,producto_precio,producto_stock,producto_foto,categoria_id,usuario_id) VALUES(:codigo,:nombre,:precio,:stock,:foto,:categoria,:usuario)");
$marcadores = [
  ":codigo" => $codigo,
  ":nombre" => $nombre,
  ":precio" => $precio,
  ":stock" => $stock,
  ":foto" => $foto,
  ":categoria" => $categoria,
  ":usuario" => $_SESSION['id']
];

$guardar_producto->execute($marcadores);
if ($guardar_producto->rowCount() == 1) {
  echo '
  <div class="alert alert-success" role="alert">
  <strong>¡PRODUCTO REGISTRADO </strong><br>
   El producto se registro  con exito
 </div>
  ';
  exit();
} else {
  if (is_file($img_dir . $foto)) {
    chmod($img_dir . $foto, 0777);
    unlink($img_dir . $foto);
  }
  echo '
  <div class="alert alert-warning" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   No se pudo registrar el producto,Por favor verifique y intente nuevamente
 </div>
  ';
}
$guardar_producto = null;
