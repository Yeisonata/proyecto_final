<?php

$inicio = ($pagina > 0) ? (($pagina * $registros) - $registros) : 0;
$tabla = "";

$campos = "producto.producto_id,producto.producto_codigo,producto.producto_nombre,producto.producto_precio,producto.producto_stock,producto.producto_foto,producto.categoria_id,producto.usuario_id,categoria.categoria_id,categoria.categoria_nombre,usuario.usuario_id,usuario.usuario_nombre,usuario.usuario_apellido";

if (isset($busqueda) && $busqueda != "") {

  $consulta_datos = "SELECT $campos FROM producto INNER JOIN categoria ON producto.categoria_id=categoria.categoria_id INNER JOIN usuario ON producto.usuario_id=usuario.usuario_id WHERE producto.producto_codigo LIKE '%$busqueda%' OR producto.producto_nombre LIKE '%$busqueda%' ORDER BY producto.producto_nombre ASC LIMIT $inicio,$registros";

  $consulta_total = "SELECT COUNT(producto_id) FROM producto WHERE producto_codigo LIKE '%$busqueda%' OR producto_nombre LIKE '%$busqueda%'";
} elseif ($categoria_id > 0) {

  $consulta_datos = "SELECT $campos FROM producto INNER JOIN categoria ON producto.categoria_id=categoria.categoria_id INNER JOIN usuario ON producto.usuario_id=usuario.usuario_id WHERE producto.categoria_id='$categoria_id' ORDER BY producto.producto_nombre ASC LIMIT $inicio,$registros";

  $consulta_total = "SELECT COUNT(producto_id) FROM producto WHERE categoria_id='$categoria_id'";
} else {

  $consulta_datos = "SELECT $campos FROM producto INNER JOIN categoria ON producto.categoria_id=categoria.categoria_id INNER JOIN usuario ON producto.usuario_id=usuario.usuario_id ORDER BY producto.producto_nombre ASC LIMIT $inicio,$registros";

  $consulta_total = "SELECT COUNT(producto_id) FROM producto";
}

$conexion = conectado();

$datos = $conexion->query($consulta_datos);
$datos = $datos->fetchAll();

$total = $conexion->query($consulta_total);
$total = (int) $total->fetchColumn();

$Npaginas = ceil($total / $registros);



if ($total >= 1 && $pagina <= $Npaginas) {
  $contador = $inicio + 1;
  $pag_inicio = $inicio + 1;
  foreach ($datos as $rows) {
    $tabla .= '
    <article class="d-flex   ">
    <figure class="text-start figure">
      <p class="" style="width: 70px;">';
      if(is_file("./img/producto/".$rows['producto_foto'])){
        $tabla .= ' <img src="./img/producto/'.$rows['producto_foto'].'" class="figure-img img-fluid rounded" alt="...">';
      }else{
        $tabla .= ' <img src="./img/caja.png" class="figure-img img-fluid rounded" alt="...">';
      }
        
        $tabla .= '</p>
    </figure>
    <div class="d-flex align-items-center">
      <div class="container ">
        <p  >
          <strong>' . $contador . ' - ' . $rows['producto_nombre'] . '</strong><br>
          <strong>CODIGO:</strong>' . $rows['producto_codigo'] . ',
          <strong>PRECIO:</strong> $' . $rows['producto_precio'] . ',
          <strong>STOCK:</strong> ' . $rows['producto_stock'] . ',
          <strong>CATEGORIA:</strong> ' . $rows['categoria_nombre'] . ',
          <strong>REGISTRADO POR:</strong>' . $rows['usuario_nombre'] . ' ' . $rows['usuario_apellido'] . '
        </p>
      </div>
      <div class="text-end d-flex  ">
        <a href="index.php?vista=producto_img&producto_id_up=' . $rows['producto_id'] . '" class="button btn btn-primary rounded-pill btn-sm">Imagen</a>

        <a href="index.php?vista=producto_update&producto_id_up=' . $rows['producto_id'] . '" class="button btn btn-warning rounded-pill btn-sm">Actualizar</a>

        <a href="' . $url . $pagina . '&producto_id_del=' . $rows['producto_id'] . '" class="button btn btn-danger rounded-pill btn-sm">Eliminar</a>
      </div>
  </article>
  <hr>
        ';
    $contador++;
  }
  $pag_final = $contador - 1;
} else {
  if ($total >= 1) {
    $tabla .= '
    <p class="text-center ">
      <a href="' . $url . '1" class="button btn btn-info rounded-pill  ">
        Haga clic acá para recargar el listado
      </a>
      </p>
			';
  } else {
    $tabla .= '<p class="text-center ">No hay registros en el sistema</p>
    ';
  }
}
if ($total > 0 && $pagina <= $Npaginas) {
  $tabla .= '
  <p class="text-end">Mostrando Productos<strong>'.$pag_inicio.'</strong> al <strong> ' . $pag_final . '</strong> de un <strong>total de ' . $total . '</strong></p>
  ';
}
$conexion = null;
echo $tabla;
if ($total >= 1 && $pagina <= $Npaginas) {
  echo paginador_tablas($pagina, $Npaginas, $url, 7);
}
