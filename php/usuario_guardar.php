<?php
require_once "main.php";

#Almacenado de Datos$
$nombre = limpiar_cadena($_POST['usuario_nombre']);
$apellido = limpiar_cadena($_POST['usuario_apellido']);

$usuario = limpiar_cadena($_POST['usuario_usuario']);
$email = limpiar_cadena($_POST['usuario_email']);


$clave_1 = limpiar_cadena($_POST['usuario_clave_1']);
$clave_2 = limpiar_cadena($_POST['usuario_clave_2']);

#Verificador de datos#

if ($nombre == "" || $apellido == "" || $usuario == "" || $clave_1 == "" || $clave_2 == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}
#Verificador De integridad de Datos

if (verificar_datos("[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}", $nombre)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Nombre no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}", $apellido)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El APELLIDO no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9]{4,20}", $usuario)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El USUARIO no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9$@.-]{7,100}", $clave_1) || verificar_datos("[a-zA-Z0-9$@.-]{7,100}", $clave_2)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 Las Claves  no cumplen las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}

#Verificar Correo# 
if ($email != "") {
  if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //para verificiar si no existe un correo duplicado
    $check_email = conectado();
    //SELECT PARA LLAMAR DATOS EMAIL DE LA BASE DE DATOS
    $check_email = $check_email->query("SELECT usuario_email	FROM usuario WHERE usuario_email='$email'");
    if ($check_email->rowCount() > 0) {
      echo '
      <div class="alert alert-primary" role="alert">
      <strong>¡Ocurrio un error inesperado</strong><br>
       El correo ingresado ya se encuentra registrado,Verifique email  ingresar nuevamente
     </div>
      ';
      exit();
    }
    //liberar espacio en  memoria
    $check_email = null;
  } else {
    echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     El Correo eletronico  no es valido,verifique y intente nuevamente.
   </div>
    ';
    exit();
  }
}
#Verificiar Usuario#
$check_usuario = conectado();
$check_usuario = $check_usuario->query("SELECT usuario_usuario	FROM usuario WHERE usuario_usuario='$usuario'");
if ($check_usuario->rowCount() > 0) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   El Usuario ingresado ya se encuentra registrado,Verifique el usuario  ingresar nuevamente
 </div>
  ';
  exit();
}
//liberar espacio en  memoria
$check_usuario = null;

#Metodo para comprobar que las claves sean las mismas #
if ($clave_1 != $clave_2) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   Las Claves ingresadas no  coniciden ,Verifique las contraseñas  nuevamente
 </div>
  ';
  exit();
} else {
  // para encriptar la clave
  $clave = password_hash($clave_1, PASSWORD_BCRYPT, ["const" => 10]);
}

#Guardar Datos#
$guardar_usuario = conectado();
#para evitar inyecion mysql
$guardar_usuario = $guardar_usuario->prepare("INSERT INTO usuario(usuario_nombre,	usuario_apellido,	usuario_usuario	,usuario_clave	,usuario_email	)  VALUES(:nombre,:apellido,:usuario,:clave,:email)");
$marcadores = [
  ":nombre" => $nombre,
  ":apellido" => $apellido,
  ":usuario" => $usuario,
  ":clave" => $clave,
  ":email" => $email
];
$guardar_usuario->execute($marcadores);
if ($guardar_usuario->rowCount() == 1) {
  echo '
  <div class="alert alert-success" role="alert">
  <strong>¡USUARIO  REGISTRADO </strong><br>
   El usuario se registro  con exito
 </div>
  ';
  exit();
} else {
  echo '
  <div class="alert alert-warning" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   No se pudo registrar el usuario,Por favor verifique y intente nuevamente
 </div>
  ';
}
$guardar_usuario = null;
