<?php

$inicio = ($pagina > 0) ? (($pagina * $registros) - $registros) : 0;
$tabla = "";

if (isset($busqueda) && $busqueda != "") {

  $consulta_datos = "SELECT * FROM categoria WHERE categoria_nombre LIKE '%$busqueda%' OR categoria_ubicacion  LIKE '%$busqueda%' ORDER BY categoria_nombre ASC LIMIT $inicio,$registros";

  $consulta_total = "SELECT COUNT(categoria_id) FROM categoria WHERE categoria_nombre LIKE '%$busqueda%' OR categoria_ubicacion  LIKE '%$busqueda%'";
} else {

  $consulta_datos = "SELECT * FROM categoria ORDER BY categoria_nombre ASC LIMIT $inicio,$registros";

  $consulta_total = "SELECT COUNT(categoria_id) FROM categoria";
}

$conexion = conectado();

$datos = $conexion->query($consulta_datos);
$datos = $datos->fetchAll();

$total = $conexion->query($consulta_total);
$total = (int) $total->fetchColumn();

$Npaginas = ceil($total / $registros);

$tabla .= '
	<div class="table">
    <table class=" table  table-bordered is-striped is-narrow is-hoverable is-fullwidth">
      <thead>
        <tr class="col  text-center">
          <th>#</th>
          <th>Nombres</th>
          <th>Ubicacion</th>
          <th>Producto</th>
          <th colspan="2">Opciones</th>
        </tr>
      </thead>
      <tbody>
	';

if ($total >= 1 && $pagina <= $Npaginas) {
  $contador = $inicio + 1;
  $pag_inicio = $inicio + 1;
  foreach ($datos as $rows) {
    $tabla .= '
    <tr class="col">
    <td>' . $contador . '</td>
    <td>' . $rows['categoria_nombre'] . '</td>
    <td>' . substr($rows['categoria_ubicacion'],0,25). '</td>
    <td>
    <a href="index.php?vista=producto_categoria&categoria_id='.$rows['categoria_id'] .'"class="button btn btn-info rounded-pill ">Ver Productos</a>
    </td>
    <td>
    <a href="index.php?vista=categoria_update&categoria_id_up='.$rows['categoria_id'].'"   class="button btn btn-success rounded-pill ">Actualizar</a>
    </td>
    <td>
    <a href="'.$url.$pagina.'&categoria_id_del='.$rows['categoria_id'].'" class="button btn btn-danger rounded-pill ">Eliminar</a>
    </td>
  </tr>
            ';
    $contador++;
  }
  $pag_final = $contador - 1;
} else {
  if ($total >= 1) {
    $tabla .= '
    <tr class="col text-center">
    <td colspan="6">
      <a href="' . $url . '1" class="button btn btn-info rounded-pill  ">
        Haga clic acá para recargar el listado
      </a>
    </td>
  </tr>
			';
  } else {
    $tabla .= '
    <tr class="col text-center">
    <td colspan="6">
      No hay registros en el sistema
    </td>
  </tr>';
  }
}


$tabla .= '</tbody></table></div>';
if ($total > 0 && $pagina <= $Npaginas) {
  $tabla .= '
  <p class="text-end">Mostrando Categoria<strong>'.$pag_inicio.'</strong> al <strong> ' . $pag_final . '</strong> de un <strong>total de ' . $total . '</strong></p>
  ';
}
$conexion = null;
echo $tabla;
if ($total >= 1 && $pagina <= $Npaginas) {
  echo paginador_tablas($pagina, $Npaginas, $url, 7);
}
