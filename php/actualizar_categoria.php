
<?php


require_once "main.php";

/*== Almacenando id ==*/
$id = limpiar_cadena($_POST['categoria_id']);

/*== Verificando categoria ==*/
$check_categoria = conectado();
$check_categoria= $check_categoria->query("SELECT * FROM categoria WHERE categoria_id='$id'");

if ($check_categoria->rowCount() <= 0) {
  echo '
            
            <div class="alert alert-danger" role="alert">
        <strong>¡Ocurrio un error inesperado</strong><br>
        La categoria no existe en el sistema
       </div>
        ';
  exit();
} else {
  $datos = $check_categoria->fetch();
}
$check_categoria = null;

#Almacenado de Datos$
$nombre = limpiar_cadena($_POST['categoria_nombre']);
$ubicacion = limpiar_cadena($_POST['categoria_ubicacion']);
if ($nombre == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}


if (verificar_datos("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{4,50}", $nombre)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Nombre no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if ($ubicacion != "") {
  if (verificar_datos("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{5,150}", $ubicacion)) {
    echo '
   <div class="alert alert-danger" role="alert">
   <strong>¡Ocurrio un error inesperado</strong><br>
   La UBICACION no cumple las condiciones,Verifique y intente nuevamente.
  </div>
   ';
    exit();
  }
}
#Verificiar nombre#
if($nombre!=$datos['categoria_nombre']){
  $check_nombre = conectado();
  $check_nombre = $check_nombre->query("SELECT categoria_nombre	FROM categoria WHERE categoria_nombre='$nombre'");
  if ($check_nombre->rowCount() > 0) {
    echo '
    <div class="alert alert-primary" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     El NOMBRE ingresado ya se encuentra registrado,Por favor elija otro!!
   </div>
    ';
    exit();
  }
  //liberar espacio en  memoria
  $check_nombre = null;
}





/*== Actualizar datos ==*/
$actualizar_categoria = conectado();
$actualizar_categoria = $actualizar_categoria->prepare("UPDATE categoria SET categoria_nombre=:nombre,categoria_ubicacion=:ubicacion WHERE categoria_id=:id");


$marcadores = [
  ":nombre" => $nombre,
  ":ubicacion" => $ubicacion,
  ":id" => $id
];

if ($actualizar_categoria->execute($marcadores)) {
  echo '
      <div class="alert alert-success" role="alert">
      <strong>¡CATEGORIA ACTUALIZADA!</strong><br>
      La categoria se actualizo con exito
     </div>
      ';
} else {
  echo '
      <div class="alert alert-danger" role="alert">
      <strong>¡Ocurrio un error inesperado!</strong><br>
      No se pudo actualizar la categoria,por favor intente nuevamente
     </div>
      ';
}
$actualizar_categoria = null;

