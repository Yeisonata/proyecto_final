<?php
require_once "main.php";
$id = limpiar_cadena($_POST['producto_id']);
/*== Verificando  producto==*/
$check_producto = conectado();
$check_producto= $check_producto->query("SELECT * FROM producto WHERE producto_id='$id'");

if ($check_producto->rowCount() <= 0) {
  echo '
            
            <div class="alert alert-danger" role="alert">
        <strong>¡Ocurrio un error inesperado</strong><br>
        El producto no existe en el sistema
       </div>
        ';
  exit();
} else {
  $datos = $check_producto->fetch();
}
$check_producto = null;

#Almacenado de Datos$
$codigo = limpiar_cadena($_POST['producto_codigo']);
$nombre = limpiar_cadena($_POST['producto_nombre']);

$precio = limpiar_cadena($_POST['producto_precio']);
$stock = limpiar_cadena($_POST['producto_stock']);
$categoria = limpiar_cadena($_POST['producto_categoria']);

#Verificador de datos#
if ($codigo == "" || $nombre == "" || $precio == "" || $stock == "" || $categoria == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}

#Verificador De integridad de Datos

if (verificar_datos("[a-zA-Z0-9]{1,70}", $codigo)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El  Codigo  de barra no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ().,$#\-\/  ]{1,70}", $nombre)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Nombre no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[0-9.]{1,25}", $precio)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Precio no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[0-9 ]{1,25}", $stock)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Stock no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
#Verificiar codigo#

if($codigo!=$datos['producto_codigo']){
  $check_codigo = conectado();
  $check_codigo = $check_codigo->query("SELECT producto_codigo	FROM producto WHERE producto_codigo='$codigo'");
  if ($check_codigo->rowCount() > 0) {
    echo '
    <div class="alert alert-primary" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     El  CODIGO de BARRAS  ingresado ya se encuentra registrado,Verifique el codigo  ingresar nuevamente
   </div>
    ';
    exit();
  }
  //liberar espacio en  memoria
$check_codigo = null;
}


#Verificiar  Nombre#
if($nombre!=$datos['producto_nombre']){
  $check_nombre = conectado();
  $check_nombre = $check_nombre->query("SELECT 	producto_nombre FROM producto WHERE 	producto_nombre ='$nombre'");
  if ($check_nombre->rowCount() > 0) {
    echo '
    <div class="alert alert-primary" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     El NOMBRE del PRODUCTO  ingresado ya se encuentra registrado,Verifique el usuario  ingresar nuevamente
   </div>
    ';
    exit();
  }
  //liberar espacio en  memoria
  $check_nombre = null;
}

if($categoria!=$datos['categoria_id']){
  #Verificiar Categoria#
$check_categoria = conectado();
$check_categoria = $check_categoria->query("SELECT categoria_id	FROM categoria  WHERE categoria_id='$categoria'");
if ($check_categoria->rowCount() <= 0) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   La categoria seleccionada no existe.
 </div>
  ';
  exit();
}
//liberar espacio en  memoria
$check_categoria = null;
}

 /*== Actualizando datos ==*/
 $actualizar_producto=conectado();
 $actualizar_producto=$actualizar_producto->prepare("UPDATE producto SET producto_codigo=:codigo,producto_nombre=:nombre,producto_precio=:precio,producto_stock=:stock,categoria_id=:categoria WHERE producto_id=:id");

$marcadores=[
  ":codigo"=>$codigo,
  ":nombre"=>$nombre,
  ":precio"=>$precio,
  ":stock"=>$stock,
  ":categoria"=>$categoria,
  ":id"=>$id
];

if ($actualizar_producto->execute($marcadores)) {
  echo '
      <div class="alert alert-success" role="alert">
      <strong>¡PRODUCTO ACTUALIZADO!</strong><br>
      La categoria se actualizo con exito
     </div>
      ';
} else {
  echo '
      <div class="alert alert-danger" role="alert">
      <strong>¡Ocurrio un error inesperado!</strong><br>
      No se pudo actualizar el producto,por favor intente nuevamente
     </div>
      ';
}
$actualizar_producto = null;
