<?php
$producto_id_del= limpiar_cadena($_GET['producto_id_del']);

//verficadon producto
$check_producto = conectado();
$check_producto = $check_producto->query("SELECT * FROM producto WHERE producto_id='$producto_id_del'");
if ($check_producto->rowCount() == 1 ) {
  $datos=$check_producto->fetch();

  $eliminar_producto = conectado();
  $eliminar_producto = $eliminar_producto->prepare("DELETE FROM producto WHERE producto_id=:id");

  $eliminar_producto->execute([":id" => $producto_id_del]);

  if ($eliminar_producto->rowCount() == 1) {
    if(is_file("./img/producto/".$datos['producto_foto'])){
       chmod("./img/producto/".$datos['producto_foto'],0777);
       unlink("./img/producto/".$datos['producto_foto']);
    }
    echo '
      <div class="alert alert-info" role="alert">
      <strong>¡PRODUCTO  ELIMINADO </strong><br>
      El producto se elimino  con exito
     </div>
      ';
  } else {
    echo '
        
          <div class="alert alert-warning" role="alert">
          <strong>¡Ocurrio un error inesperado</strong><br>
          No se pudo eliminar el producto, por favor intente nuevamente
         </div>
      ';
  }
  $eliminar_producto = null;
} else {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
El Producto no existe!!
 </div>
  ';
}
$check_producto= null;