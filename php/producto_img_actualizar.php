<?php
require_once "main.php";
$producto_id = limpiar_cadena($_POST['img_up_id']);

/*== Verificando  producto==*/
$check_producto = conectado();
$check_producto = $check_producto->query("SELECT * FROM producto WHERE producto_id='$producto_id'");

if ($check_producto->rowCount() == 1) {
  $datos = $check_producto->fetch();
} else {
  echo '
            <div class="alert alert-danger" role="alert">
        <strong>¡Ocurrio un error inesperado</strong><br>
        La imagen del producto no existe en el sistema
       </div>
        ';
  exit();
}
$check_producto = null;

#Directorio de Imagenes# 
$img_dir  = "../img/producto/";

#comprobar si se selecciono una imagen

if ($_FILES['producto_foto']['name']=="" || $_FILES['producto_foto']['size']==0) {
  echo '
            
  <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
    No ha seleccionado ninguna imagen valida
  </div>
';
  exit();
}
if (!file_exists($img_dir)) {
  // permisos de lectura y escritura
  if (!mkdir($img_dir, 0777)) {
    echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
    Error al crear el directorio
  </div>
    ';
    exit();
  }
}
//para dar permisos de lectura y escritura
chmod($img_dir, 0777);

//Verificar el formato de las imagenes
if (mime_content_type($_FILES['producto_foto']['tmp_name']) != "image/jpeg" && mime_content_type($_FILES['producto_foto']['tmp_name']) != "image/png") {
  echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio   un error inesperado</strong><br>
   La imagen que ha seleccionado es de formato no permitido.
  </div>
    ';
  exit();
}

//Verificador del peso de las imagenes
if (($_FILES['producto_foto']['size'] / 1024) > 3072) {
  echo '
    <div class="alert alert-danger" role="alert">
          <strong>¡Ocurrio   un error inesperado</strong><br>
            La imagen que ha seleccionado supera el peso  permitido.
    </div>
    ';
  exit();
}
//Para saber que tipo de imagen es
switch (mime_content_type($_FILES['producto_foto']['tmp_name'])) {
  case 'image/jpeg':
    $img_ext = ".jpg";
    break;
  case 'image/png':
    $img_ext = ".png";
    break;
}

$img_nombre = renombrar_fotos($datos['producto_nombre']);
$foto = $img_nombre . $img_ext;

//Para mover la imagen al directorio
if (!move_uploaded_file($_FILES['producto_foto']['tmp_name'], $img_dir . $foto)) {
  echo '
    <div class="alert alert-danger" role="alert">
          <strong>¡Ocurrio   un error inesperado</strong><br>
            No podemos subir la imagen al sistema en este momento.
    </div>
    ';
  exit();
}

//para borrar la foto anterior
if (is_file($img_dir . $datos['producto_foto']) && $datos['producto_foto'] != $foto) {
  chmod($img_dir . $datos['producto_foto'], 0777);
  unlink($img_dir . $datos['producto_foto']);
}

$actualizar_producto=conectado();
    $actualizar_producto=$actualizar_producto->prepare("UPDATE producto SET producto_foto=:foto WHERE producto_id=:id");

    $marcadores=[
        ":foto"=>$foto,
        ":id"=>$producto_id
    ];
if ($actualizar_producto->execute($marcadores)) {
  echo '
      <div class="alert alert-success" role="alert">
        <strong>¡IMAGEN ELIMINADA!</strong><br>
        La imagen del prodcuto ha sido actualizada con exito
        Presione aceptar para volver.
      </div>
      <p class="text-center pt-5 pb-5" >
       <a  href="index.php?vista=producto_img&producto_id_up=' . $producto_id . '" class="button btn btn-warning">Aceptar</a></p>
      ';
} else {
  if (is_file($img_dir . $foto)) {
    chmod($img_dir . $foto, 0777);
    unlink($img_dir . $foto);
  }
  echo '
      <div class="alert alert-danger" role="alert">
        <strong>Ocurrio un error</strong><br>
        No podemos subir la imagen en este momento, Por favor
        intente nuevamente
       </div>
      ';
}
$actualizar_producto = null;
