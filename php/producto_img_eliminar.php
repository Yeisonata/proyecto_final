<?php
require_once "main.php";
$producto_id = limpiar_cadena($_POST['img_del_id']);

/*== Verificando  producto==*/
$check_producto = conectado();
$check_producto = $check_producto->query("SELECT * FROM producto WHERE producto_id='$producto_id'");

if ($check_producto->rowCount() == 1) {
  $datos = $check_producto->fetch();
} else {
  echo '
            
            <div class="alert alert-danger" role="alert">
        <strong>¡Ocurrio un error inesperado</strong><br>
        La imagen del producto no existe en el sistema
       </div>
        ';
  exit();
}
$check_producto = null;
#Directorio de Imagenes# 
$img_dir  = "../img/producto/";
chmod($img_dir, 0777);

if (is_file($img_dir . $datos['producto_foto'])) {
  chmod($img_dir . $datos['producto_foto'], 0777);
  if (!unlink($img_dir . $datos['producto_foto'])) {
    echo '
            
    <div class="alert alert-danger" role="alert">
        <strong>¡Ocurrio un error inesperado</strong><br>
          Error al intentar eliminar la imagen del producto del sistema.Por favor verifique intente nuevamente
      </div>  
';
  }
}
$actualizar_producto=conectado();
$actualizar_producto=$actualizar_producto->prepare("UPDATE producto SET producto_foto=:foto WHERE producto_id=:id");

$marcadores=[
  ":foto"=>"",
  ":id"=>$producto_id 
];
if ($actualizar_producto->execute($marcadores)) {
  echo '
      <div class="alert alert-success" role="alert">
        <strong>¡IMAGEN ELIMINADA!</strong><br>
        La imagen del prodcuto se borro con exito
        Presione aceptar para volver.
      </div>
      <p class="text-center" pt-5 pb-5>
       <a  href="index.php?vista=producto_img&producto_id_up='.$producto_id.'" class="button btn btn-warning">Aceptar</a>
      ';
} else {
  echo '
      <div class="alert alert-danger" role="alert">
        <strong>¡IMAGEN ELIMINADA!</strong><br>
        Estimado usuario Hubo un error, pero se pudo eliminar la imagen del producto
        ha sido eliminada,Presione aceptar para volver.
       </div>

       <p class="text-center" pt-5 pb-5>
       <a  href="index.php?vista=producto_img&producto_id_up='.$producto_id.'" class="button btn btn-warning">Aceptar</a>

       </p>
      ';
}
$actualizar_producto = null;