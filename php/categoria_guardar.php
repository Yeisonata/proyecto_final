<?php
require_once "main.php";
#Almacenado de Datos$
$nombre = limpiar_cadena($_POST['categoria_nombre']);
$ubicacion = limpiar_cadena($_POST['categoria_ubicacion']);
if ($nombre == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{4,50}", $nombre)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El Nombre no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if ($ubicacion != "") {
  if (verificar_datos("[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]{5,150}", $ubicacion)) {
    echo '
   <div class="alert alert-danger" role="alert">
   <strong>¡Ocurrio un error inesperado</strong><br>
   La UBICACION no cumple las condiciones,Verifique y intente nuevamente.
  </div>
   ';
    exit();
  }
}
#Verificiar nombre#
$check_nombre = conectado();
$check_nombre = $check_nombre->query("SELECT categoria_nombre	FROM categoria WHERE categoria_nombre='$nombre'");
if ($check_nombre->rowCount() > 0) {
  echo '
  <div class="alert alert-primary" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   El NOMBRE ingresado ya se encuentra registrado,Por favor elija otro!!
 </div>
  ';
  exit();
}
//liberar espacio en  memoria
$check_nombre = null;



#Guardar Datos#
$guardar_categoria = conectado();
#para evitar inyecion mysql
$guardar_categoria = $guardar_categoria->prepare("INSERT INTO categoria(categoria_nombre,categoria_ubicacion)  VALUES(:nombre,:ubicacion)");
$marcadores =[
  ":nombre"=>$nombre,
  ":ubicacion"=>$ubicacion
  
];
$guardar_categoria->execute($marcadores);

if ($guardar_categoria->rowCount()==1) {
  echo '
  <div class="alert alert-success" role="alert">
  <strong>¡CATEGORIA  REGISTRADA!</strong><br>
 La Categoria se registro  con exito
 </div>
  ';
} else {
  echo '
  <div class="alert alert-warning" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
   No se pudo registrar la categoria,Por favor verifique ,intente nuevamente
 </div>
  ';
}

$guardar_categoria = null;
