<?php
require_once "../inc/session_star.php";

require_once "main.php";

/*== Almacenando id ==*/
$id = limpiar_cadena($_POST['usuario_id']);

/*== Verificando usuario ==*/
$check_usuario = conectado();
$check_usuario = $check_usuario->query("SELECT * FROM usuario WHERE usuario_id='$id'");

if ($check_usuario->rowCount() <= 0) {
  echo '
            
            <div class="alert alert-danger" role="alert">
        <strong>¡Ocurrio un error inesperado</strong><br>
        El usuario no existe en el sistema
       </div>
        ';
  exit();
} else {
  $datos = $check_usuario->fetch();
}
$check_usuario = null;

/*== Almacenando datos del administrador ==*/
$admin_usuario = limpiar_cadena($_POST['administrador_usuario']);
$admin_clave = limpiar_cadena($_POST['administrador_clave']);
if ($admin_usuario == "" || $admin_clave == "") {
  echo '
      <div class="alert alert-danger" role="alert">
      <strong>¡Ocurrio un error inesperado</strong><br>
      No has llenado todos los campos obligatorios
    </div>
      ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9]{4,20}", $admin_usuario)) {
  echo '
     <div class="alert alert-danger" role="alert">
     <strong>¡Ocurrio un error inesperado</strong><br>
    SU USUARIO no cumple las condiciones,Verifique y intente nuevamente.
    </div>
     ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9$@.-]{7,100}",  $admin_clave)) {
  echo '
     <div class="alert alert-danger" role="alert">
     <strong>¡Ocurrio un error inesperado</strong><br>
    SU CLAVE no cumple las condiciones,Verifique y intente nuevamente.
    </div>
     ';
  exit();
}
/*== Verificando el administrador en DB ==*/
$check_admin = conectado();
$check_admin = $check_admin->query("SELECT usuario_usuario,usuario_clave FROM usuario WHERE usuario_usuario='$admin_usuario' AND usuario_id='" . $_SESSION['id'] . "'");
if ($check_admin->rowCount() == 1) {

  $check_admin = $check_admin->fetch();

  if ($check_admin['usuario_usuario'] != $admin_usuario || !password_verify($admin_clave, $check_admin['usuario_clave'])) {
    echo '
	            
              <div class="alert alert-danger" role="alert">
              <strong>¡Ocurrio un error inesperado</strong><br>
              USUARIO o CLAVE de administrador incorrectos
             </div>
	        ';
    exit();
  }
} else {
  echo '
      <div class="alert alert-danger" role="alert">
      <strong>¡Ocurrio un error inesperado</strong><br>
      USUARIO o CLAVE de administrador incorrectos
     </div>
        ';
  exit();
}
$check_admin = null;

$nombre = limpiar_cadena($_POST['usuario_nombre']);
$apellido = limpiar_cadena($_POST['usuario_apellido']);

$usuario = limpiar_cadena($_POST['usuario_usuario']);
$email = limpiar_cadena($_POST['usuario_email']);


$clave_1 = limpiar_cadena($_POST['usuario_clave_1']);
$clave_2 = limpiar_cadena($_POST['usuario_clave_2']);
if ($nombre == "" || $apellido == "" || $usuario == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}
if (verificar_datos("[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}", $nombre)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El APELLIDO no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,40}", $apellido)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El APELLIDO no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9]{4,20}", $usuario)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El USUARIO no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
#Verificar Correo# 
if ($email != "" && $email != $datos['usuario_email']) {
  if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //para verificiar si no existe un correo duplicado
    $check_email = conectado();
    //SELECT PARA LLAMAR DATOS EMAIL DE LA BASE DE DATOS
    $check_email = $check_email->query("SELECT usuario_email	FROM usuario WHERE usuario_email='$email'");
    if ($check_email->rowCount() > 0) {
      echo '
      <div class="alert alert-primary" role="alert">
      <strong>¡Ocurrio un error inesperado</strong><br>
       El correo ingresado ya se encuentra registrado,Verifique email  ingresar nuevamente
     </div>
      ';
      exit();
    }
    //liberar espacio en  memoria
    $check_email = null;
  } else {
    echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     El Correo eletronico  no es valido,verifique y intente nuevamente.
   </div>
    ';
    exit();
  }
}
#Verificiar Usuario#
if ($usuario != $datos['usuario_usuario']) {
  $check_usuario = conectado();
  $check_usuario = $check_usuario->query("SELECT usuario_usuario	FROM usuario WHERE usuario_usuario='$usuario'");
  if ($check_usuario->rowCount() > 0) {
    echo '
    <div class="alert alert-primary" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     El Usuario ingresado ya se encuentra registrado,Verifique email  ingresar nuevamente
   </div>
    ';
    exit();
  }
  $check_usuario = null;
}
#Metodo para comprobar que las claves sean las mismas #
if ($clave_1 != "" || $clave_2 != "") {
  if (verificar_datos("[a-zA-Z0-9$@.-]{7,100}", $clave_1) || verificar_datos("[a-zA-Z0-9$@.-]{7,100}", $clave_2)) {
    echo '
   <div class="alert alert-danger" role="alert">
   <strong>¡Ocurrio un error inesperado</strong><br>
   Las Claves  no cumplen las condiciones,Verifique y intente nuevamente.
  </div>
   ';
    exit();
  } else {
    if ($clave_1 != $clave_2) {
      echo '
      <div class="alert alert-primary" role="alert">
      <strong>¡Ocurrio un error inesperado</strong><br>
       Las Claves ingresadas no  coniciden ,Verifique las contraseñas  nuevamente
     </div>
      ';
      exit();
    } else {
      // para encriptar la clave
      $clave = password_hash($clave_1, PASSWORD_BCRYPT, ["const" => 10]);
    }
  }
} else {
  $clave = $datos['usuario_clave'];
}

/*== Actualizar datos ==*/
$actualizar_usuario = conectado();
$actualizar_usuario = $actualizar_usuario->prepare("UPDATE usuario SET usuario_nombre=:nombre,usuario_apellido=:apellido,usuario_usuario=:usuario,usuario_clave=:clave,usuario_email=:email WHERE usuario_id=:id");


$marcadores = [
  ":nombre" => $nombre,
  ":apellido" => $apellido,
  ":usuario" => $usuario,
  ":clave" => $clave,
  ":email" => $email,
  ":id" => $id
];

if ($actualizar_usuario->execute($marcadores)) {
  echo '
      <div class="alert alert-success" role="alert">
      <strong>¡USUARIO ACTUALIZADO!</strong><br>
      El usuario se actualizo con exito
     </div>
      ';
} else {
  echo '
      <div class="alert alert-danger" role="alert">
      <strong>¡Ocurrio un error inesperado!</strong><br>
      No se pudo actulizar el usuario,por favor intente nuevamente
     </div>
      ';
}
$actualizar_usuario = null;
