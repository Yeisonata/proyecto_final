<?php

$categoria_id_del= limpiar_cadena($_GET['categoria_id_del']);

//verficadon categoria
$check_categoria = conectado();
$check_categoria = $check_categoria->query("SELECT categoria_id FROM categoria WHERE categoria_id='$categoria_id_del'");
if ($check_categoria->rowCount() == 1) {

  $check_productos = conectado();
  $check_productos = $check_productos->query("SELECT categoria_id FROM producto WHERE categoria_id='$categoria_id_del' LIMIT 1");

  if ($check_productos->rowCount() <= 0) {

    $eliminar_categoria = conectado();
    $eliminar_categoria = $eliminar_categoria->prepare("DELETE FROM categoria WHERE categoria_id=:id");

    $eliminar_categoria->execute([":id" => $categoria_id_del]);

    if ($eliminar_categoria->rowCount() == 1) {
      echo '
        <div class="alert alert-info" role="alert">
        <strong>¡PRODUCTO  ELIMINADO </strong><br>
        lLa categoria se elimino  con exito
       </div>
        ';
    } else {
      echo '
          
            <div class="alert alert-warning" role="alert">
            <strong>¡Ocurrio un error inesperado</strong><br>
            No se pudo eliminar la categoria, por favor intente nuevamente
           </div>
        ';
    }
    $eliminar_categoria = null;
  } else {
    echo '
        
          <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
    No podemos eliminar la categoria ya que tiene productos registrados en el sistema
   </div>
      ';
  }
  $check_productos = null;
} else {
  echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
  la Categoria no existe!!
   </div>
    ';
}
$check_categoria = null;
