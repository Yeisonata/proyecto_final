<?php
$usuario = limpiar_cadena($_POST['login_usuario']);
$clave = limpiar_cadena($_POST['login_clave']);
if ($usuario == "" || $clave == "") {
  echo '
  <div class="alert alert-danger" role="alert">
  <strong>¡Ocurrio un error inesperado</strong><br>
  No has llenado todos los campos obligatorios
</div>
  ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9]{4,20}", $usuario)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 El usuario no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
if (verificar_datos("[a-zA-Z0-9$@.-]{7,100}", $clave)) {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
 La  Clave no cumple las condiciones,Verifique y intente nuevamente.
</div>
 ';
  exit();
}
$check_user = conectado();
$check_user = $check_user->query("SELECT * FROM usuario WHERE usuario_usuario='$usuario'");

if ($check_user->rowCount() == 1) {
  $check_user = $check_user->fetch();
  //para verificar si el usuario  coincide con de el de la bases de datos y la contraseña
  if ($check_user['usuario_usuario'] == $usuario && password_verify($clave, $check_user['usuario_clave'])) {

    $_SESSION['id'] = $check_user['usuario_id'];
    $_SESSION['nombre'] = $check_user['usuario_nombre'];
    $_SESSION['apellido'] = $check_user['usuario_apellido'];
    $_SESSION['usuario'] = $check_user['usuario_usuario'];

    if (headers_sent()) {
      echo "<script>window.location.href='index.php?vista=home';</script>";
    } else {
      header("Location: index.php?vista=home");
    }
  } else {
    echo '
    <div class="alert alert-danger" role="alert">
    <strong>¡Ocurrio un error inesperado</strong><br>
     Usuario o Clave estan incorrectos,Verifique e intente nuevamente
   </div>
    ';
  }
} else {
  echo '
 <div class="alert alert-danger" role="alert">
 <strong>¡Ocurrio un error inesperado</strong><br>
  Usuario o Clave estan incorrectos,Verifique e intente nuevamente
</div>
 ';
}
$check_user = null;
