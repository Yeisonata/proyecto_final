<?php
//PARA CONECTAR A LA BASE DE DATOS
function conectado()
{
  $con = new PDO('mysql:host=localhost:3305;dbname=hospital', 'root', '');
  return $con;
}
// para verificar los datos que si  se cumplan
function verificar_datos($filtro, $cadena)
{
  if (preg_match("/^" . $filtro . "$/", $cadena)) {
    return false;
  } else {
    return true;
  }
}
# Limpiar cadenas de textos#
function limpiar_cadena($cadena)
{
  #quitar los espacios al momento de enviar
  $cadena = trim($cadena);
  # quirar # / . en el texto
  $cadena = stripslashes($cadena);
  #evitar inyeccion sql y ataques xsess
  $cadena = str_ireplace("<script>", "", $cadena);
  $cadena = str_ireplace("</script>", "", $cadena);
  $cadena = str_ireplace("<script src", "", $cadena);
  $cadena = str_ireplace("<script type=", "", $cadena);
  $cadena = str_ireplace("SELECT * FROM", "", $cadena);
  $cadena = str_ireplace("DELETE FROM", "", $cadena);
  $cadena = str_ireplace("INSERT INTO", "", $cadena);
  $cadena = str_ireplace("DROP TABLE", "", $cadena);
  $cadena = str_ireplace("DROP DATABASE", "", $cadena);
  $cadena = str_ireplace("TRUNCATE TABLE", "", $cadena);
  $cadena = str_ireplace("SHOW TABLES;", "", $cadena);
  $cadena = str_ireplace("SHOW DATABASES;", "", $cadena);
  $cadena = str_ireplace("<?php", "", $cadena);
  $cadena = str_ireplace("?>", "", $cadena);
  $cadena = str_ireplace("--", "", $cadena);
  $cadena = str_ireplace("^", "", $cadena);
  $cadena = str_ireplace("<", "", $cadena);
  $cadena = str_ireplace("[", "", $cadena);
  $cadena = str_ireplace("]", "", $cadena);
  $cadena = str_ireplace("==", "", $cadena);
  $cadena = str_ireplace(";", "", $cadena);
  $cadena = str_ireplace("::", "", $cadena);
  $cadena = trim($cadena);
  $cadena = stripslashes($cadena);
  return $cadena;
}

# Funcion renombrar Fotos
function renombrar_fotos($nombre)
{
  $nombre = str_ireplace(" ", "_", $nombre);
  $nombre = str_ireplace("/", "_", $nombre);
  $nombre = str_ireplace("#", "_", $nombre);
  $nombre = str_ireplace("_", "_", $nombre);
  $nombre = str_ireplace("$", "_", $nombre);
  $nombre = str_ireplace(".", "_", $nombre);
  $nombre = str_ireplace(",", "_", $nombre);
  $nombre = $nombre . "_" . rand(0, 100);
  return $nombre;
}

#Funcion paginador de tablas #
function paginador_tablas($pagina, $Npaginas, $url, $botones)
{
  $tabla = '
<nav aria-label="Page navigation example">
';


  if ($pagina <= 1) {
    $tabla .= '
  <ul class="pagination">
  <li class="page-item"><a class="page-link disabled">Anterior</a></li>
  ';
  } else {
    $tabla .= '
  
  <li class="page-item"><a class="page-link" href="' . $url . ($pagina - 1) . '">Anterior</a></li>
  <ul class="pagination">
  <li class="page-item"><a class="page-link" href="' . $url . '">1</a></li>
  ';
  }
  $ci = 0;
  for ($i = $pagina; $i <= $Npaginas; $i++) {
    if ($ci >= $botones) {
      break;
    }
    if ($pagina == $i) {
      $tabla .= '
        <li class="page-item"><a class="page-link" href="' . $url . $i . '">' . $i . '</a></li>
        ';
    } else {
      $tabla .= '
        <li class="page-item"><a class="page-link" href="' . $url . $i . '">' . $i . '</a></li>
        ';
    }
    $ci++;
  }

  if ($pagina == $Npaginas) {
    $tabla .= '
      <li class="page-item"><a class="page-link ">Siguiente</a></li>
      </ul>
			';
  } else {
    $tabla .= '
      <li class="page-item"><a class="page-link" href="' . $url . ($pagina + 1) . '">' . $Npaginas . '</a></li>
      <li class="page-item"><a class="page-link" href="#">"' . $url . $Npaginas . '</a></li>
      </ul>
			';
  }
  $tabla .= '
</nav>
';
  return $tabla;
}
